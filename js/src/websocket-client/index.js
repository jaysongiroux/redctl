// Copyright 2019 Assured Information Security, Inc.

const defaultConnection = 'ws://localhost:' + global.backendPort + '/web/app/events';

const defaultOnMessage = (msg) => {
    console.log('message received: ' + msg.data);
};

class Event {
    constructor(name, handler) {
        if (typeof name !== 'string') {
            console.error('event name must be string, got: ' + typeof name);
            return;
        }

        if (typeof handler !== 'function') {
            console.error('event handler must be function, got: ' + typeof handler);
            return;
        }

        this.name = name;
        this.handler = handler;
    }
}

export default class WebsocketClient {
    constructor(connection) {
        if (!connection) {
            connection = defaultConnection;
        }

        this.socket = new WebSocket(connection);

        this.events = [];

        this.socket.onopen = () => {
            console.log('WebSocket connection to "' + connection + '" established');

            this.sendMessage({ event: 'ready' });
        };

        this.socket.onmessage = defaultOnMessage;
    }

    receive() {
        this.socket.onmessage = (msg) => {
            defaultOnMessage(msg);

            let payload = JSON.parse(msg.data);

            let event = this.events.find((e) => (e.name === payload.event));

            if (event) {
                event.handler(payload);
            }
        };
    }

    sendMessage(obj) {
        this.socket.send(JSON.stringify(obj));
    }

    handle(name, handler) {
        let event = new Event(name, handler);

        if (event.name && event.handler) {
            this.events.push(event);
        }
    }

    sendEvent(name, payload) {
        if (typeof name !== 'string') {
            console.error('event name must be string, got: ' + typeof name);
            return;
        }

        this.sendMessage({ event: name, data: payload });
    }
}
