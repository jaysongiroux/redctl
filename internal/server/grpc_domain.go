// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/google/uuid"

	"gitlab.com/redfield/redctl/api"
)

func (s *server) DomainCreate(ctx context.Context, in *api.DomainCreateRequest) (*api.DomainCreateReply, error) {
	domain := *in.Domain

	domain.Uuid = uuid.New().String()

	// Make sure that the Uuid is set in the domain configuration also
	cfg := domain.GetConfig()
	cfg.Uuid = domain.Uuid

	log.Printf("opened db collection - creating: %+v", domain)

	err := s.domainRepo.Create(&domain)
	if err != nil {
		log.Printf("failed to create domain: %v %v\n", domain, err)
		return nil, fmt.Errorf("failed to create domain: %v %v", domain, err)
	}

	log.Printf("created domain: %+v\n", domain)

	// Some subscribers may need domain configuration information.
	s.publishDomainEvent(api.Event_DOMAIN_CREATED, &domain)

	return &api.DomainCreateReply{Domain: &domain}, nil
}

func (s *server) DomainFind(ctx context.Context, in *api.DomainFindRequest) (*api.DomainFindReply, error) {
	domain, err := s.domainRepo.Find(in.GetUuid())
	if err != nil {
		log.Printf("failed to find domain: %v %v\n", in.GetUuid(), err)
		return nil, fmt.Errorf("failed to find domain: %v %v", in.GetUuid(), err)
	}

	log.Printf("find domain: %+v\n", domain)

	return &api.DomainFindReply{Domain: domain}, nil
}

func (s *server) DomainFindAll(ctx context.Context, in *api.DomainFindAllRequest) (*api.DomainFindAllReply, error) {
	domains, err := s.domainRepo.FindAll()
	if err != nil {
		log.Printf("failed to find all domains: %v\n", err)
		return nil, fmt.Errorf("failed to find all domains: %v", err)
	}

	log.Printf("findall domains: %+v\n", domains)

	return &api.DomainFindAllReply{Domains: domains}, nil
}

func (s *server) DomainUpdate(ctx context.Context, in *api.DomainUpdateRequest) (*api.DomainUpdateReply, error) {
	domain := *in.Domain

	log.Printf("opened db collection - updating: %+v", domain)

	err := s.domainRepo.Update(&domain)
	if err != nil {
		log.Printf("failed to update domain: %v %v\n", domain, err)
		return nil, fmt.Errorf("failed to update domain: %v %v", domain, err)
	}

	log.Printf("updated domain: %+v\n", domain)

	return &api.DomainUpdateReply{Domain: &domain}, nil
}

func (s *server) DomainRemove(ctx context.Context, in *api.DomainRemoveRequest) (*api.DomainRemoveReply, error) {
	id := in.Uuid

	log.Printf("opened db collection - removing: %v", id)

	err := s.domainRepo.Remove(id)
	if err != nil {
		log.Printf("failed to remove domain: %v %v\n", id, err)
		return nil, fmt.Errorf("failed to remove domain: %v %v", id, err)
	}

	log.Printf("removed domain: %v\n", id)

	// Some subscribers may need domain configuration information.
	s.publishDomainEvent(api.Event_DOMAIN_REMOVED, &api.Domain{Uuid: id})

	return &api.DomainRemoveReply{}, nil
}

func (s *server) domainStartWithDependencies(domain *api.Domain) error {
	domains, err := s.domainRepo.FindAll()
	if err != nil {
		return err
	}

	// Domain dependencies are determined by examining network backends
	var deps []*api.Domain
	for _, n := range domain.GetConfig().GetNetworks() {
		name := n.GetBackend()
		// TODO: Add logic to find dependency loops in the more general case,
		//       e.g. `vm0 -> vm1 -> vm2 -> vm0`
		if name == domain.GetConfig().GetName() {
			return fmt.Errorf("dependency loop: domain %v", name)
		}

		for _, dom := range domains {
			if name == dom.GetConfig().GetName() {
				deps = append(deps, dom)
			}
		}
	}

	for _, dep := range deps {
		if err := s.domainStartWithDependencies(dep); err != nil {
			return err
		}
	}

	if s.xenDriver.IsDomainCreated(*domain) {
		// Nothing more to do!
		return nil
	}

	err = s.xenDriver.StartDomain(*domain)
	if err != nil {
		return err
	}

	s.publishDomainEvent(api.Event_DOMAIN_STARTED, domain)

	// Wait until we can verify the domain is created, up to a timeout.
	timeout := 3 * time.Second
	if s.opts != nil && s.opts.DomainStartTimeout != 0 {
		timeout = s.opts.DomainStartTimeout
	}

	for t := time.After(timeout); ; {
		select {
		case <-t:
			return fmt.Errorf("domain failed to hit run state after %s", timeout)
		default:
			if s.xenDriver.IsDomainCreated(*domain) {
				err = s.notifyDomainResolutionChange(domain, "")
				if err != nil {
					log.Printf("failed to set domain resolution: %v\n", err)
				}

				return nil
			}
		}
	}
}

func (s *server) DomainStart(ctx context.Context, in *api.DomainStartRequest) (*api.DomainStartReply, error) {
	domain, err := s.domainRepo.Find(in.GetUuid())
	if err != nil {
		return nil, fmt.Errorf("unable to start domain: %v", err)
	}

	err = s.domainStartWithDependencies(domain)
	if err != nil {
		return nil, fmt.Errorf("unable to start domain: %v", err)
	}

	return &api.DomainStartReply{}, nil
}

func (s *server) DomainStop(ctx context.Context, in *api.DomainStopRequest) (*api.DomainStopReply, error) {
	domain, err := s.domainRepo.Find(in.GetUuid())
	if err != nil {
		return nil, fmt.Errorf("unable to stop domain: %v", err)
	}

	err = s.xenDriver.StopDomain(*domain)
	if err != nil {
		return nil, err
	}

	s.publishDomainEvent(api.Event_DOMAIN_STOPPED, domain)

	return &api.DomainStopReply{}, nil
}

func (s *server) Shutdown(ctx context.Context, in *api.ShutdownRequest) (*api.ShutdownReply, error) {
	timeout := in.GetTimeout()
	if timeout == 0 {
		timeout = 60
	}

	s.shutdown(timeout)

	return &api.ShutdownReply{}, nil
}

func (s *server) DomainRestart(ctx context.Context, in *api.DomainRestartRequest) (*api.DomainRestartReply, error) {
	domain, err := s.domainRepo.Find(in.GetUuid())
	if err != nil {
		return nil, fmt.Errorf("unable to restart domain: %v", err)
	}

	err = s.xenDriver.RestartDomain(*domain)
	if err != nil {
		return nil, err
	}

	return &api.DomainRestartReply{}, nil
}

func (s *server) domainNetworkAttach(domain *api.Domain, network *api.DomainNetwork) error {
	if err := s.xenDriver.NetworkAttach(*domain, *network); err != nil {
		return fmt.Errorf("unable to attach network to domain: %v", err)
	}

	s.publishDomainNetworkEvent(api.Event_NETWORK_ATTACH, domain, network)

	return nil
}

func (s *server) DomainHotplugNetworkAttach(ctx context.Context, in *api.DomainHotplugNetworkAttachRequest) (*api.DomainHotplugNetworkAttachReply, error) {
	err := s.domainNetworkAttach(in.GetDomain(), in.GetNetwork())
	if err != nil {
		return nil, err
	}

	return &api.DomainHotplugNetworkAttachReply{}, nil
}

func (s *server) domainNetworkDetach(domain *api.Domain, network *api.DomainNetwork) error {
	if err := s.xenDriver.NetworkDetach(*domain, *network); err != nil {
		return fmt.Errorf("unable to detach network from domain: %v", err)
	}

	s.publishDomainNetworkEvent(api.Event_NETWORK_DETACH, domain, network)

	return nil
}

func (s *server) DomainHotplugNetworkDetach(ctx context.Context, in *api.DomainHotplugNetworkDetachRequest) (*api.DomainHotplugNetworkDetachReply, error) {
	err := s.domainNetworkDetach(in.GetDomain(), in.GetNetwork())
	if err != nil {
		return nil, err
	}

	return &api.DomainHotplugNetworkDetachReply{}, nil
}

func (s *server) DomainHotplugPciAttach(ctx context.Context, in *api.DomainHotplugPciAttachRequest) (*api.DomainHotplugPciAttachReply, error) {
	domain, err := s.domainRepo.Find(in.GetUuid())
	if err != nil {
		return nil, fmt.Errorf("unable to attach pci device to domain: %v", err)
	}

	p := in.GetPcidev()

	if err := s.xenDriver.PCIDeviceAttach(*domain, *p); err != nil {
		return nil, fmt.Errorf("unable to attach pci device to domain: %v", err)
	}

	return &api.DomainHotplugPciAttachReply{}, nil
}

func (s *server) DomainHotplugPciDetach(ctx context.Context, in *api.DomainHotplugPciDetachRequest) (*api.DomainHotplugPciDetachReply, error) {
	domain, err := s.domainRepo.Find(in.GetUuid())
	if err != nil {
		return nil, fmt.Errorf("unable to detach pci device from domain: %v", err)
	}

	p := in.GetPcidev()

	if err := s.xenDriver.PCIDeviceDetach(*domain, *p); err != nil {
		return nil, fmt.Errorf("unable to detach pci device from domain: %v", err)
	}

	return &api.DomainHotplugPciDetachReply{}, nil
}
