// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"os/user"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"gitlab.com/redfield/redctl/api"
)

type xenDriver struct{}

func envDisplay() string {
	// If there are any errors, just return empty string. This is a best-effort
	// to correctly configure DISPLAY. If it fails let the error propagate to xl.
	//
	// `who` will produce output that looks something like:
	// root       :0           2019-02-28 15:23 (:0)
	// root       pts/2        2019-02-28 15:29 (:0)
	//
	// The last column shows the DISPLAY variable. This function examines lines
	// of the output that contain the username, and then uses a regular expression to
	// extract the string inside of the parenthesis.
	out, err := exec.Command("who").CombinedOutput()
	if err != nil {
		return ""
	}

	u, err := user.Current()
	if err != nil {
		return ""
	}
	username := u.Username

	for _, line := range strings.Split(string(out), "\n") {
		if strings.Contains(line, username) {
			r := regexp.MustCompile(`\((.*?)\)`)
			display := r.FindStringSubmatch(line)
			if len(display) >= 2 {
				return display[1]
			}
		}
	}

	return ""
}

func envXAuthority() string {
	u, err := user.Current()
	if err != nil {
		return ""
	}

	return fmt.Sprintf("/run/user/%v/gdm/Xauthority", u.Uid)
}

func envConfigure(cmd *exec.Cmd) {
	if ev := envDisplay(); ev != "" {
		cmd.Env = append(cmd.Env, fmt.Sprintf("DISPLAY=%v", ev))
	}

	if ev := envXAuthority(); ev != "" {
		cmd.Env = append(cmd.Env, fmt.Sprintf("XAUTHORITY=%v", ev))
	}
}

func (xd *xenDriver) IsDomainCreated(domain api.Domain) bool {
	rd, err := xd.RunningDomainsByName()
	if err != nil {
		// Cannot be determined
		return false
	}

	for _, name := range rd {
		if domain.GetConfig().GetName() == name {
			return true
		}
	}

	return false
}

// parseExtractSpecString with spec <disk>[,<partition>]:<path>
// partition is returned as -1 when unused
func parseExtractSpecString(extract string) (disk int, partition int, path string, err error) {
	disk = -1
	partition = -1
	path = ""

	spl := strings.Split(extract, ":")

	if len(spl) != 2 {
		return disk, partition, path, fmt.Errorf("failed to parse image path: " + extract)
	}

	path = spl[1]

	spl = strings.Split(spl[0], ",")
	disk, err = strconv.Atoi(spl[0])
	if err != nil {
		return disk, partition, path, fmt.Errorf("failed to parse image path: " + extract)
	}

	if len(spl) == 2 {
		partition, err = strconv.Atoi(spl[1])
		if err != nil {
			return disk, partition, path, fmt.Errorf("failed to parse image path: " + extract)
		}
	}

	if len(spl) > 2 {
		return disk, partition, path, fmt.Errorf("failed to parse image path: " + extract)
	}

	return disk, partition, path, nil
}

func copyFile(src, dst string) (int64, error) {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}

func findAvailableNbd() (string, error) {
	cmd := exec.Command("modprobe", "nbd")
	if out, err := cmd.CombinedOutput(); err != nil {
		return "", fmt.Errorf("modprobe nbd failed (err=%v): %v", err, string(out))
	}

	block := "/sys/class/block"
	files, err := ioutil.ReadDir(block)
	if err != nil {
		return "", fmt.Errorf("failed to read /sys/class/block: %v", err)
	}

	for _, file := range files {
		if !strings.HasPrefix(file.Name(), "nbd") {
			continue
		}

		nbd := filepath.Join(block, file.Name())
		spath := filepath.Join(nbd, "size")
		content, err := ioutil.ReadFile(spath)
		if err != nil {
			log.Printf("failed to read %v: %v", spath, err)
			continue
		}

		size, err := strconv.Atoi(strings.TrimSpace(string(content)))
		if err != nil {
			log.Printf("failed to parse size (%v) at %v: %v", size, spath, err)
			continue
		}

		if size == 0 {
			return filepath.Join("/dev", file.Name()), nil
		}
	}

	return "", fmt.Errorf("failed to find available nbd device")
}

var nbdMux sync.Mutex

func qemuNbdConnect(source string) (string, error) {
	// To prevent race issues, lock nbd connection handling. This is a
	// relatively short operation, so shouldn't be a problem.
	nbdMux.Lock()
	defer nbdMux.Unlock()

	nbd, err := findAvailableNbd()
	if err != nil {
		return "", fmt.Errorf("failed to find nbd device: %v", err)
	}

	err = exec.Command("qemu-nbd", "--connect", nbd, "-r", source).Run()
	if err != nil {
		return "", fmt.Errorf("qemu-nbd connect failed (err=%v)", err)
	}

	log.Printf("qemu-nbd connected %v: %v\n", nbd, source)
	return nbd, nil
}

func qemuNbdDisconnect(nbd string) error {
	cmd := exec.Command("qemu-nbd", "--disconnect", nbd)
	if out, err := cmd.CombinedOutput(); err != nil {
		return fmt.Errorf("qemu-nbd disconnect failed (err=%v): %v", err, string(out))
	}

	log.Printf("qemu-nbd disconnected %v\n", nbd)
	return nil
}

func mountDiskReadOnly(src, dst string) error {
	// noload option prevents loading of journal, which allows us to mount
	// dirty partitions in a read-only context.  if you do not use this,
	// you may see an error "cannot mount /dev/nbd0 read-only"
	cmd := exec.Command("mount", "-o", "ro,noload", src, dst)
	if out, err := cmd.CombinedOutput(); err != nil {
		return fmt.Errorf("failed to mount partition (err=%v): %v", err, string(out))
	}
	log.Printf("mounted read-only: %v -> %v\n", src, dst)
	return nil
}

func unmountDisk(mount string) error {
	cmd := exec.Command("umount", mount)
	if out, err := cmd.CombinedOutput(); err != nil {
		return fmt.Errorf("failed to unmount partition (err=%v): %v", err, string(out))
	}
	log.Printf("unmounted: %v\n", mount)
	return nil
}

func extractKernelImage(domain *api.Domain, extract string, opath string) error {
	disk, part, path, err := parseExtractSpecString(extract)
	if err != nil {
		return err
	}

	if disk < 0 || disk >= len(domain.Config.Disks) {
		return fmt.Errorf("failed to parse image path, disk index out of bounds (%v)", extract)
	}

	source := domain.Config.Disks[disk].Target

	nbd, err := qemuNbdConnect(source)
	if err != nil {
		return err
	}
	defer func() {
		if err := qemuNbdDisconnect(nbd); err != nil {
			log.Printf("%v", err)
		}
	}()

	if part >= 0 {
		nbd = fmt.Sprintf("%sp%d", nbd, part)
	}

	mnt, err := ioutil.TempDir("", "extract-mnt")
	if err != nil {
		return fmt.Errorf("failed to make tempdir for mount partition: %v", err)
	}
	defer os.Remove(mnt)

	if err := mountDiskReadOnly(nbd, mnt); err != nil {
		return err
	}
	defer func() {
		if err := unmountDisk(mnt); err != nil {
			log.Printf("%v", err)
		}
	}()

	_, err = copyFile(filepath.Join(mnt, path), opath)
	if err != nil {
		return fmt.Errorf("failed to copy file: %v", err)
	}

	log.Printf("extracted %s to=%s\n", source, opath)
	return nil
}

func (xd *xenDriver) StartDomain(domain api.Domain) error {
	if xd.IsDomainCreated(domain) {
		return nil
	}

	if domain.Config.GetKernelExtract() != "" {
		kpath := "/tmp/kernel-" + domain.Uuid
		err := extractKernelImage(&domain, domain.Config.KernelExtract, kpath)
		if err != nil {
			return err
		}
		domain.Config.Kernel = kpath
	}

	if domain.Config.GetRamdiskExtract() != "" {
		rpath := "/tmp/ramdisk-" + domain.Uuid
		err := extractKernelImage(&domain, domain.Config.RamdiskExtract, rpath)
		if err != nil {
			return err
		}
		domain.Config.Ramdisk = rpath
	}

	data, err := domain.Config.Serialize()
	if err != nil {
		return err
	}

	path := fmt.Sprintf("/tmp/xl-%v", domain.GetUuid())
	if err := ioutil.WriteFile(path, data, 0644); err != nil {
		return err
	}

	logPath := fmt.Sprintf("/tmp/xl-create-%v.log", domain.Config.GetName())
	logFile, err := os.Create(logPath)
	if err != nil {
		return err
	}

	cmd := exec.Command("xl", "create", "-d", path)
	cmd.Stderr = logFile
	envConfigure(cmd)

	if err := cmd.Start(); err != nil {
		logFile.Close()
		return err
	}

	go func() {
		defer logFile.Close()
		if err := cmd.Wait(); err != nil {
			log.Printf("xl create did not complete successfully")
		}
	}()

	return nil
}

func (xd *xenDriver) StopDomain(domain api.Domain) error {
	// If domain isn't running, don't do anything
	if !xd.IsDomainCreated(domain) {
		return nil
	}

	done := make(chan bool)

	go func() {
		for {
			select {
			case <-done:
				return
			default:
				// Sometimes a VM needs repeated ACPI events to
				// shutdown, e.g. when its asleep
				time.Sleep(1 * time.Second)

				// We don't care about the error, ignore on lint
				// nolint
				exec.Command("xl", "shutdown", "-F", domain.GetConfig().GetName()).Run()
			}
		}
	}()

	err := exec.Command("xl", "shutdown", "-F", "--wait", domain.GetConfig().GetName()).Run()

	done <- true

	return err
}

func (xd *xenDriver) DestroyDomain(domain api.Domain) error {
	return exec.Command("xl", "destroy", domain.GetConfig().GetName()).Run()
}

func (xd *xenDriver) RestartDomain(domain api.Domain) error {
	return exec.Command("xl", "reboot", "-F", domain.GetConfig().GetName()).Run()
}

func (xd *xenDriver) NetworkAttach(domain api.Domain, network api.DomainNetwork) error {
	text, err := network.MarshalText()
	if err != nil {
		return err
	}
	args := []string{"network-attach", domain.GetConfig().GetName()}
	args = append(args, strings.Split(string(text), ",")...)

	return exec.Command("xl", args...).Run()
}

func (xd *xenDriver) NetworkDetach(domain api.Domain, network api.DomainNetwork) error {
	name := domain.GetConfig().GetName()

	if mac := network.GetMac(); mac != "" {
		out, err := exec.Command("xl", "network-detach", name, mac).CombinedOutput()
		if err != nil {
			return fmt.Errorf("%v: %v", string(out), err)
		}

		return nil
	}

	if devid := network.GetDevid(); devid != "" {
		out, err := exec.Command("xl", "network-detach", name, devid).CombinedOutput()
		if err != nil {
			return fmt.Errorf("%v: %v", string(out), err)
		}

		return nil
	}

	// Either a devid or mac address is required
	return fmt.Errorf("either devid or mac address must be specified")
}

func (xd *xenDriver) PCIDeviceAttach(domain api.Domain, pci api.DomainPciDevice) error {
	if pci.GetAddress() == "" {
		return fmt.Errorf("pci device address is required")
	}
	name := domain.GetConfig().GetName()
	cmd := exec.Command("xl", "pci-attach", name, pci.GetAddress(), pci.GetVslot())

	return cmd.Run()
}

func (xd *xenDriver) PCIDeviceDetach(domain api.Domain, pci api.DomainPciDevice) error {
	if pci.GetAddress() == "" {
		return fmt.Errorf("pci device address is required")
	}
	name := domain.GetConfig().GetName()

	return exec.Command("xl", "pci-detach", name, pci.GetAddress()).Run()
}

// RunningDomains returns a list of names for running domains.
func (xd *xenDriver) RunningDomainsByName() ([]string, error) {
	out, err := exec.Command("xenstore-list", "/local/domain").CombinedOutput()
	if err != nil {
		return []string{}, fmt.Errorf("%v: %v", string(out), err)
	}

	domids := strings.Fields(string(out))
	domains := make([]string, len(domids))

	for i, domid := range domids {
		name, err := exec.Command("xl", "domname", domid).CombinedOutput()
		if err != nil {
			return domains, fmt.Errorf("%v: %v", string(out), err)
		}
		domains[i] = strings.TrimSpace(string(name))
	}

	return domains, nil
}
