// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"io/ioutil"
	"os"
	"testing"

	"gitlab.com/redfield/redctl/api"
	"gitlab.com/redfield/redctl/internal/server/repository"
)

type mockXenDriver struct {
	runningDomains map[string]bool
}

func (m *mockXenDriver) IsDomainCreated(domain api.Domain) bool {
	_, ok := m.runningDomains[domain.GetUuid()]

	return ok
}

func (m *mockXenDriver) StartDomain(domain api.Domain) error {
	m.runningDomains[domain.GetUuid()] = true

	return nil
}

func (m *mockXenDriver) StopDomain(domain api.Domain) error {
	delete(m.runningDomains, domain.GetUuid())

	return nil
}

func (m *mockXenDriver) StopAllDomains() error {
	m.runningDomains = make(map[string]bool)

	return nil
}

func (m *mockXenDriver) RestartDomain(domain api.Domain) error {
	return nil
}

func (m *mockXenDriver) NetworkAttach(domain api.Domain, network api.DomainNetwork) error {
	return nil
}

func (m *mockXenDriver) NetworkDetach(domain api.Domain, network api.DomainNetwork) error {
	return nil
}

func (m *mockXenDriver) PCIDeviceAttach(domain api.Domain, pci api.DomainPciDevice) error {
	return nil
}

func (m *mockXenDriver) PCIDeviceDetach(domain api.Domain, pci api.DomainPciDevice) error {
	return nil
}

func (m *mockXenDriver) RunningDomainsByName() ([]string, error) {
	return []string{}, nil
}

func (m *mockXenDriver) DestroyDomain(domain api.Domain) error {
	return nil
}

func newTestServer(t *testing.T) (*server, string) {
	scribbleDir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v\n", err)
	}

	db, err := ScribbleInitialize(scribbleDir)
	if err != nil {
		return nil, ""
	}

	domainRepo := repository.NewScribbleDomainRepo(db)
	driver := &mockXenDriver{
		runningDomains: make(map[string]bool),
	}

	return &server{xenDriver: driver, domainRepo: domainRepo}, scribbleDir
}

func newUserDomainWithNDVM(name string) *api.Domain {
	d := newUserDomain(name)
	d.AddNetwork(api.NewDomainNetwork("test-NDVM", "bridge", ""))

	return d
}

func newUserDomain(name string) *api.Domain {
	return &api.Domain{
		Config: &api.DomainConfiguration{
			Name:   name,
			Type:   "hvm",
			Vcpus:  "4",
			Memory: "4096",
		},
	}
}

func newPVDomain(name string) *api.Domain {
	return &api.Domain{
		Config: &api.DomainConfiguration{
			Name: name,
			Type: "pv",
		},
	}
}

func TestDomainStart(t *testing.T) {
	// Test starting a user domain that has
	// a dependency on a pv domain, make sure that both
	// domains come up.
	userDomain := newUserDomainWithNDVM("test-user-domain")
	backend := userDomain.GetConfig().GetNetworks()[0].GetBackend()
	pvDomain := newPVDomain(backend)

	srvr, dir := newTestServer(t)
	defer os.RemoveAll(dir)

	r := &api.DomainCreateRequest{Domain: userDomain}
	reply, err := srvr.DomainCreate(context.Background(), r)
	if err != nil {
		t.Fatalf("failed to create domain: %v", err)
	}
	userDomain = reply.GetDomain()
	defer func() {
		if err := srvr.domainRepo.Remove(userDomain.GetUuid()); err != nil {
			t.Errorf("failed to cleanup user domain")
		}
	}()

	r = &api.DomainCreateRequest{Domain: pvDomain}
	reply, err = srvr.DomainCreate(context.Background(), r)
	if err != nil {
		t.Fatalf("failed to create domain: %v", err)
	}
	pvDomain = reply.GetDomain()
	defer func() {
		if err := srvr.domainRepo.Remove(pvDomain.GetUuid()); err != nil {
			t.Errorf("failed to cleanup pv domain")
		}
	}()

	// Start the user domain
	r2 := &api.DomainStartRequest{Uuid: userDomain.GetUuid()}
	_, err = srvr.DomainStart(context.Background(), r2)
	if err != nil {
		t.Fatalf("failed to start domain: %v", err)
	}

	if !srvr.xenDriver.IsDomainCreated(*pvDomain) {
		t.Fatalf("expected domain to be started: %v", pvDomain)
	}

	if !srvr.xenDriver.IsDomainCreated(*userDomain) {
		t.Fatalf("expected domain to be started: %v", userDomain)
	}
}
