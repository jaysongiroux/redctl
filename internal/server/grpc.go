// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"fmt"
	"log"
	"net"
	"strings"

	"google.golang.org/grpc"

	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/reflection"

	"gitlab.com/redfield/redctl/api"
	"gitlab.com/redfield/redctl/internal/server/repository"
	"gitlab.com/redfield/redctl/internal/xen"
)

// server is used to implement redfield.RedctlServer.
type server struct {
	domainRepo     repository.DomainRepo
	imageRepo      repository.ImageRepo
	graphicsRepo   *repository.GraphicsRepo
	xenDriver      xen.Driver
	eventPublisher *publisher

	opts *RedctlServerOptions
}

// unix sockets not compatible with TLS it appears
var secure = false

func grpcAuthenticateClient(ctx context.Context) (string, error) {
	if md, ok := metadata.FromIncomingContext(ctx); ok {
		username := strings.Join(md["username"], "")
		password := strings.Join(md["password"], "")

		if !authenticateUser(username, password) {
			return "", fmt.Errorf("bad auth: %s %s", username, password)
		}

		log.Printf("authenticated client: %s", username)
		return username, nil
	}

	return "", fmt.Errorf("missing credentials")
}

func grpcUnaryInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	_, err := grpcAuthenticateClient(ctx)
	if err != nil {
		return nil, err
	}

	return handler(ctx, req)
}

// GrpcServe initializes and serves gRPC with specified server options (TLS, etc.)
func GrpcServe(ln net.Listener, dr repository.DomainRepo, ir repository.ImageRepo, xd xen.Driver, gr *repository.GraphicsRepo, opts *RedctlServerOptions) error {
	var gopts []grpc.ServerOption

	ep := newPublisher()

	srv := server{
		domainRepo:     dr,
		imageRepo:      ir,
		xenDriver:      xd,
		graphicsRepo:   gr,
		eventPublisher: ep,
		opts:           opts,
	}

	if err := srv.initializeHost(); err != nil {
		log.Printf("Host initialization failed: %v", err)
		return err
	}

	if secure {
		creds, err := credentials.NewServerTLSFromFile(opts.TLSCertFile, opts.TLSKeyFile)
		if err != nil {
			log.Printf("Failed to generate credentials %v", err)
			return err
		}

		gopts = append(gopts, grpc.Creds(creds))
		gopts = append(gopts, grpc.UnaryInterceptor(grpcUnaryInterceptor))
	}

	// Setup grpc server
	s := grpc.NewServer(gopts...)
	api.RegisterRedctlServer(s, &srv)
	reflection.Register(s)
	go srv.eventPublisher.serve()

	return s.Serve(ln)
}
